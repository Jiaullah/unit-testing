var assert = require("chai").assert;
var app = require("../app.js");

//these are all the unit tests for corresponding methods in app.js file
describe("AppJs file unit testing", function() {

    it("verify adddNumbers Method", function() {
        //arrange //act //assert
        var result = app.addNumbers(1, 5);
        assert.equal(result, 6);
    });

    it("verify returnHello method", function() {
        var result = app.returnHello();
        assert.equal(result, "hello");
    })

    it("verify returnHello method", function() {
        var result = app.returnHello();
        assert.typeOf(result, "String");
    })


    it("verify array method", function() {
        var result = app.returnArray();
        assert.isArray(result);
    })
});