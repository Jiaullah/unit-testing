describe("Abgular test suit", function(){
    describe("angular controller", function(){

        it("test case", function(){
            angular.mock.module('testingAngularApp');
            var scope= {};
            var ctrl;
            inject(function($controller){
                ctrl = $controller('testingAngularCtrl', {$scope:scope});
            });

            expect(scope.title).toBeDefined();
            expect(scope.title).toBe("test");
        })
    })

})