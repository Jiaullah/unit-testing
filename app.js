var appJs = function() {

    //sample methods return and these methods are used in apptest.js file

    this.returnArray = function() {
        return ["goodera", "good", "era"];
    }

    this.addNumbers = function(a, b) {
        return a + b;
    }

    this.returnHello = function() {
        return "hello";
    }

    this.returnHello1 = function() {
        return "hell";
        return "a";
    }

}

module.exports = new appJs();